from starlette.applications import Starlette
from starlette.responses import HTMLResponse, JSONResponse
from starlette.staticfiles import StaticFiles
from starlette.middleware.cors import CORSMiddleware
import uvicorn, aiohttp, asyncio
from io import BytesIO

from fastai import *
from fastai.vision import *
from transformers import *

fastai_file_url = 'https://www.dropbox.com/s/09drt12f7jqa8e7/export.pkl?raw=1'
fastai_file_name = 'models/bear.pkl'

config_json_url = 'https://www.dropbox.com/s/2rtwm3eh9j1bg4n/config.json?dl=0&raw=1'
config_json_file_name = 'models/config.json'
transformer_model_url = 'https://www.dropbox.com/s/o9hoxe7e8z5z0g0/pytorch_model.bin?dl=0&raw=1'
transformer_model_file_name = 'models/pytorch_model.bin'

path = Path(__file__).parent

app = Starlette()
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_headers=['X-Requested-With', 'Content-Type'])
app.mount('/static', StaticFiles(directory='app/static'))

async def download_file(url, dest):
    if dest.exists(): return
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            data = await response.read()
            with open(dest, 'wb') as f: f.write(data)

async def setup_fastai_learner():
    await download_file(fastai_file_url, path/fastai_file_name)
    try:
        learn = load_learner(path, fastai_file_name)
        return learn
    except RuntimeError as e:
        if len(e.args) > 0 and 'CPU-only machine' in e.args[0]:
            print(e)
            message = "\n\nThis model was trained with an old version of fastai and will not work in a CPU environment.\n\nPlease update the fastai library in your training environment and export your model again.\n\nSee instructions for 'Returning to work' at https://course.fast.ai."
            raise RuntimeError(message)
        else:
            raise

async def setup_transformer():
    await download_file(config_json_url, path/config_json_file_name)
    await download_file(transformer_model_url, path/transformer_model_file_name)
    try:
        model = DistilBertForSequenceClassification.from_pretrained(path/'models/')
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        return (model,tokenizer)
    except RuntimeError as e:
        raise

def process_text(text:str, model, preprocessor):
   inputs = preprocessor.encode(text,return_tensors='pt',add_special_tokens=True)
   return  model(inputs)[0][0]

loop = asyncio.get_event_loop()
tasks_fastai = [asyncio.ensure_future(setup_fastai_learner())]
fastai_learner = loop.run_until_complete(asyncio.gather(*tasks_fastai))[0]
tasks_transformers = [asyncio.ensure_future(setup_transformer())]
transformer,tokenizer = loop.run_until_complete(asyncio.gather(*tasks_transformers))[0]
loop.close()

@app.route('/')
def index(request):
    html = path/'view'/'index.html'
    return HTMLResponse(html.open().read())

@app.route('/more_info')
def index(request):
    html = path/'view'/'more_info.html'
    return HTMLResponse(html.open().read())


@app.route('/analyze_img', methods=['POST'])
async def analyze_img(request):
    data = await request.form()
    img_bytes = await (data['file'].read())
    img = open_image(BytesIO(img_bytes))
    predicted_class, class_id, confidence = fastai_learner.predict(img)
    return JSONResponse({
        'result': str(predicted_class),
        'confidence': confidence.tolist()
        })

@app.route('/analyze_text', methods=['POST'])
async def analyze_text(request):
    data = await request.form()
    text = data['text']
    # add the special tokens to the extracted text, as BERT expects them
    # out_prediction = process_text('[CLS] ' + text + ' [SEP]',transformer,tokenizer) # new in 2.2.0, tokenizer add them by default
    # https://github.com/huggingface/transformers/releases/tag/v2.2.0
    pred = process_text(text,transformer,tokenizer)
    return JSONResponse({
        'result': pred.argmax().item(),
        'confidence': torch.softmax(pred,0).tolist()
        })

if __name__ == '__main__':
    if 'serve' in sys.argv: uvicorn.run(app=app, host='0.0.0.0', port=5042)
