var el = x => document.getElementById(x);

function showPicker(inputId) { el('file-input').click(); }

function showPicked(input) {
    el('upload-label').innerHTML = input.files[0].name;
    el('upload-label').classList.add('no-display');
    var reader = new FileReader();
    reader.onload = function (e) {
        el('image-picked').src = e.target.result;
        el('image-picked').className = '';
    }
    reader.readAsDataURL(input.files[0]);
}

function analyze_img() {
    var uploadFiles = el('file-input').files;
    if (uploadFiles.length != 1){
        alert('Please select 1 file to analyze!');
        return;
    }

    el('analyze-button').innerHTML = 'Analyzing...';
    el('bear-graph').classList.remove('no-display');

    var xhr = new XMLHttpRequest();
    var loc = window.location
    xhr.open('POST', `${loc.protocol}//${loc.hostname}:${loc.port}/analyze_img`, true);
    xhr.onerror = function() {alert (xhr.responseText);}
    xhr.onload = function(e) {
        if (this.readyState === 4) {
            var response = JSON.parse(e.target.responseText);
            el('result-label').innerHTML = `That's a ${response['result']} bear!`;
            myBearChart.data.datasets[0].data = response['confidence'];
            myBearChart.update();

        }
        el('analyze-button').innerHTML = 'Analyze';
    }

    var fileData = new FormData();
    fileData.append('file', uploadFiles[0]);
    xhr.send(fileData);
}

function analyze_text() {
    var uploadText = el('text-input').value;
    if (uploadText.length < 1){
        alert('Please insert some text to analyze!');
        return;
    } 

    el('analyze-button-text').innerHTML = 'Analyzing...';
    el('nlp-graph').classList.remove('no-display');

    var xhr = new XMLHttpRequest();
    var loc = window.location
    xhr.open('POST', `${loc.protocol}//${loc.hostname}:${loc.port}/analyze_text`, true);
    xhr.onerror = function() {alert (xhr.responseText);}
    xhr.onload = function(e) {
        if (this.readyState === 4) {
            var response = JSON.parse(e.target.responseText);
            // el('result-label-text').innerHTML = `${response['result']}`;
            myNLPChart.data.datasets[0].data = response['confidence'].reverse();
            myNLPChart.update();
        }
        el('analyze-button-text').innerHTML = 'Analyze';
    }

    var textData = new FormData();
    textData.append('text', uploadText);
    xhr.send(textData);
}

