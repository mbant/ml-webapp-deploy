document.getElementById('text-input').addEventListener('keyup', (e) => {
    if(e.which === 13 || e.which === 32)
        analyze_text();
});


const bear_ctx = document.getElementById('bear-chart').getContext('2d');
const myBearChart = new Chart(bear_ctx, {
    type: 'horizontalBar',
    data: {
        labels: ['Black  Bear', 'Grizzly Bear','Polar Bear', 'Teddy Bear'],
        datasets: [{
            label: 'Model Predictions',
            data: [0,0,0,0],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

const nlp_ctx = document.getElementById('nlp-chart').getContext('2d');
const myNLPChart = new Chart(nlp_ctx, {
    type: 'horizontalBar',
    data: {
        labels: ['Positive', 'Negative'],
        datasets: [{
            label: 'Model Predictions',
            data: [0, 0],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)', // blue
                // 'rgba(75, 192, 192, 0.2)', // green
                'rgba(255, 99, 132, 0.2)' // red
                // 'rgba(255, 206, 86, 0.2)', // yellow
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                // 'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)'
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});