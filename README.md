## WIP: 

This repo is undergoing a quite substancial change in the [`tutorial`](https://gitlab.com/mbant/ml-webapp-deploy/tree/tutorial) branch, where I'll post notebooks for the working process of building a new version of this from start to finish, training on [AWS EC2](https://aws.amazon.com/machine-learning/amis/) instances and deploying on [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/); back soon with news! 🎉

# Starter for deploying ML models on the web.

Because models in a jupyter notebook are not actionable, deployed models are.

This repo can be used as a starting point to deploy ML models on any Web hosting platform that supports docker, like [AWS Elastic Beanstalk](https://aws.amazon.com/), [Google App Engine](https://cloud.google.com/appengine/) or [Render](https://render.com), amongst many other...
The starter code used an image classifier built with the [fast.ai](https://github.com/fastai/fastai) library, which was slightly modified here and then I added a NLP sentiment analysis model (build using the [🤗 Huggingface](https://github.com/huggingface/transformers) library).

You can see the code in production [here](https://coming-soon).

<img alt="nlp-gif" src="./images/NLP.gif" height="700" width="700"> 

To test locally clone the repo, make sure you have all requirements installed, and run it with

```bash
git clone git@gitlab.com:mbant/ml-webapp-deploy.git
python3 -m pip install --user --upgrade -r requirements.txt
python3 app/server.py serve
```
and open `localhost:5042` with your favorite browser.

*Thanks to Simon Willison for [sample](https://github.com/render-examples/fastai-v3) code.*
